package edu.agh.sius.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import edu.agh.sius.bean.Form;

import java.util.LinkedList;
import java.util.List;

/**
 * Kontroler odpowiedzialny za tworzenie i listowanie formularzy.
 */
@Controller
public class FormController {

    private List<Form> forms = new LinkedList<Form>();

    // lista formularzy
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getFormList() {
        return new ModelAndView("list", "forms", forms);
    }

    // foremka do tworzenia nowego formularza
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView getEmptyForm() {
		ModelAndView modelAndView = new ModelAndView("create", "command", new Form());
        modelAndView.addObject("inputTypes", Form.INPUT_TYPES);
        return modelAndView;
	}

    // tworzenie formularzy
	@RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView createForm(@ModelAttribute("create") Form form, BindingResult result) {
        forms.add(form);
        return new ModelAndView("show", "form", form);
    }

    // prezentowanie formularza za pomocą HTML i JS
    @RequestMapping(value = "/show/{formId}", method = RequestMethod.GET)
    public ModelAndView showForm(@PathVariable String formId) {
        return new ModelAndView("show", "form", forms.get(Integer.parseInt(formId)));
    }

}