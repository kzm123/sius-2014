package edu.agh.sius.bean;

import java.util.Scanner;

/**
 * Klasa reprezentująca formularz - jego pola i walidacją stosowaną do tych pól.
 * Pola formularza przechowywane są w zmiennej "data" - każda nowa linia to osobne pole.
 * Walidacja w analogiczny sposób.
 */
public class Form {

    // dostępne typy danych przechowywanych w formularzach
    public static final String[] INPUT_TYPES = {
        "color", "date", "datetime", "datetime-local", "email", "month", "number", "range",
        "search", "tel", "time", "url", "week"
    };
    
	private String name;
	private String data;
    private String validation;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
    public String getValidation() {
        return validation;
    }
    public void setValidation(String validation) {
        this.validation = validation;
    }
    public String getHtmlData() {
        String html = "";
        Scanner scanner = new Scanner(data);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.contains(",")) {
                String[] attrs = line.split(",");
                String element = attrs[0];
                String type = attrs[1];
                String name = attrs[2];
                String label = attrs[3];
                html += "<label for='id_" + name + "'>" + label + "</label>";
                html += "<" + element + " id='id_" + name + "' name='" + name + "' type='" + type + "' />";
            }
        }
        scanner.close();
        return html;
    }
    public String getJsValidation() {
        String js = "";
        Scanner scanner = new Scanner(validation);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.contains(",")) {
                String[] attrs = line.split(",");
                String type = attrs[0];
                String name = attrs[1];
                if (type.equals("range")) {
                    String min = attrs[2];
                    String max = attrs[3];
                    js += "$('[name=" + name + "]').change(function() {" +
                            "if ($(this).val() < " + min + " || $(this).val() > " + max + ") {" +
                                "$(this).addClass('error');" +
                            "} else {" +
                                "$(this).removeClass('error');" +
                            "}" +
                          "});";
                }
            }
        }
        scanner.close();
        return js;
    }

}
