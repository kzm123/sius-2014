<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!doctype html>
<html>
<head>
<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/main.js" />"></script>
</head>
<body>

    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <a class="brand" href="/">Form Builder</a>
            <ul class="nav">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="create.html">Create</a></li>
            </ul>
        </div>
    </div>
    
    <div class="container">

        <script>
            $(function() {
                ${form.jsValidation}
            });
        </script>
	
		<div class="row">
			<h2>${form.name}</h2>
			<div id="form-data">${form.htmlData}</div>
		</div>
		
	</div>
	
</body>
</html>