<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!doctype html>
<html>
<head>
<link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/main.js" />"></script>
</head>
<body>

    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <a class="brand" href="/">Form Builder</a>
            <ul class="nav">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="create.html">Create</a></li>
            </ul>
        </div>
    </div>
    
    <div class="container">
		<form:form method="POST" id="form" commandName="command" action="create.html">
		    <div style="">
		    	<label for="id_form_name">Form name: </label>
			    <form:input path="name" id="id_form_name" />
			    <div class="<%--hide--%>">
                    <form:textarea path="data" />
                    <form:textarea path="validation" />
                </div>
		    </div>
		    <input type="submit" value="Save" class="btn btn-primary" />
		</form:form>
		
		<div class="row">
			<div id="toolbox" class="span4">
				<h2>Add input</h2>

                <label for="id_input_label">Label: </label>
				<input id="id_input_label" name="input_label" type="text" />

                <label for="id_input_label">Name: </label>
				<input id="id_input_name" name="input_name" type="text" /><br>

                <label for="id_input_type">Type: </label>
                <select id="id_input_type" name="input_type">
                    <c:forEach items="${inputTypes}" var="type">
                        <option value="${type}">${type}</option>
                    </c:forEach>
				</select>

                <div id="validation_inputs">
                    <div data-type="number" style="display: none">
                        <label for="id_input_value_min">Min: </label>
                        <input id="id_input_value_min" name="value_min" type="text" /><br>

                        <label for="id_input_value_max">Max: </label>
                        <input id="id_input_value_max" name="value_max" type="text" /><br>
                    </div>
                </div>

                <br>
				<a href="#add-input" class="btn" style="float: left" data-type="text">Add</a>
			</div>
			
			<div class="span8">
				<h2>Preview</h2>
				<div id="preview" class="span8">
				</div>
			</div>
		</div>
    </div>
	
</body>
</html>