$(function($) {
	var $toolbox = $('#toolbox'),
		$addInput = $toolbox.find('[href="#add-input"]'),
		$formPreview = $('#preview'),
		$form = $('#form'),
        $validationInputs = $('#validation_inputs');
	
	$addInput.click(function(event) {
		event.preventDefault();
		var $label = $toolbox.find('input[name="input_label"]'),
            label = $label.val(),
            $name = $toolbox.find('input[name="input_name"]'),
			name = $name.val(),
			type = $toolbox.find('select[name="input_type"]').val(),
            data = $form.find('[name="data"]').val(),
            validationData = $form.find('[name="validation"]').val();

        if (!name.length || !label.length) {
            if (!name.length)
                $name.addClass('error');
            if (!label.length)
                $label.addClass('error');
        } else {
            $('<label for="id_input_' + name + '">' + label + ': </label>' +
                '<input id="id_input' + name + '" name="' + name + '" type="' + type + '" />').appendTo($formPreview);
            $form.find('[name="data"]').val(data + 'input,' + type + ',' + name + ',' + label + '\n');
            if (type == 'number') {
                var min = $validationInputs.find('input[name="value_min"]').val(),
                    max = $validationInputs.find('input[name="value_max"]').val();
                if (min && max) {
                    $form.find('[name="validation"]').val(validationData + 'range,' + name + ',' + min + ',' + max + '\n');
                } else {
                    $form.find('[name="validation"]').val(validationData + '\n');
                }
            } else {
                $form.find('[name="validation"]').val(validationData + '\n');
            }
        }
	});

    $toolbox.find('select[name="input_type"]').change(function() {
        var val = $(this).val();
        $validationInputs.childrenw().hide();
        $validationInputs.find('[data-type="' + val + '"]').show();
    });
	
});
